from adafruit_servokit import ServoKit
import time
import RPi.GPIO as GPIO

kit = ServoKit(channels=16)
oe = 22 # Pin to Disable the Servos
GPIO_TRIGGER = 23 #sonic sensor trigger
GPIO_ECHO = 24 #sonic sensor echo
GPIO.setmode(GPIO.BCM)
GPIO.setup(oe, GPIO.OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

#Start from the bottom up for the arm for each servo (Optional Use)
BASEJOINT = 0
JOINTA = 1
JOINTB = 2
JOINTC = 3
JOINTD = 4
GRIPPER = 5

pos0 = 10
pos2 = 40
pos3 = 175
pos4 = 170

def distance():
    GPIO.output(GPIO_TRIGGER, True)

    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    test_start_time = time.time()
    pulse_end_time = test_start_time

    #save StartTime
    while (GPIO.input(GPIO_ECHO) == 0 and (time.time() - test_start_time) < 0.1):
        pulse_start_time = time.time()

    #save StopTime
    while (GPIO.input(GPIO_ECHO) == 1 and (time.time() - test_start_time) < 0.1):
        pulse_end_time = time.time()

    pulse_duration = pulse_end_time - pulse_start_time
    
    #multiply with sonic speed of 34300cm/s
    #divide by 2, because there and back
    distance = round(pulse_duration * 17160,2)
    return distance


# This function allows for the control of all servos unlike the rest below
# servAng value range -1 to 1. (i.e 0.1, -0.5)  * use for throttle *
# servAng value range 0 to 180.                 * use for angle *
# ** NOTE : Use of kit.servo[n].angle and kit.continuous_servo[n].throttle
#           can not be used at the same time as they are meant to be used
#           for two different servo types. i.e continuous_servo or positional
def servCtrl(servAng,timeSlp,servNum):

  # Uncomment line below to use for continuouspositional servos
  # Comment out line below if using positional servos 
  #kit.continuous_servo[servNum].throttle = servAng 

  # Uncomment line below to use for positional servos
  # Comment out line below if using continuous servos
  # Can still be used for continuous servos
  kit.servo[servNum].angle = servAng
  time.sleep(timeSlp)


def on():
    GPIO.output(oe, GPIO.LOW)

def off():
    GPIO.output(oe, GPIO.HIGH)

#make changing angle smooth because no encoders
def drive(start,stop,delay,servo_num):
    if(start < stop):
      for x in range (start, stop):
        kit.servo[servo_num].angle = x
        time.sleep(delay)
    else:
        for x in range (start, stop, -1):
            kit.servo[servo_num].angle = x
            time.sleep(delay)

#FOR RESTING
#servo 0 resting is 10  -- 100 to 150 -- 70 is straight up
#servo 1 resting is 90  -- 50 is straight up --WEAK
#servo 2 resting is 70  -- 100
#servo 3 resting is 120 -- 70 is straight up
#servo 4 is 90 because its the rotating wrist??

def do_it():
    #setup
    servCtrl(90,1,5) #gripper
    servCtrl(50,1,1) 
    servCtrl(90,1,2)
    servCtrl(70,1,3)
    servCtrl(90,1,4)
    drive(10,100,0.035,0) #motor 0 going down

def first_position():
    pos0 = 10
    pos2 = 40
    pos3 = 80
    pos4 = 175
    drive(pos0,90,0.035,0)

    servCtrl(pos2,1,2)
    servCtrl(pos3,1,3)
    servCtrl(pos4,1,4)

    servCtrl(100,1,5)

def rest():
    drive(90,10,0.035,0)

def grab():
  drive(160,140,0.04,3) 
  servCtrl(95,1,5)
  grab = False
  dist_count = 0  
  for x in range (0,70):
    dist = distance()
    if (dist<3 and dist > 0):
        dist_count = dist_count + 1  
    print("doing the loop")
        
    print(dist)
    if(dist > 7):
        kit.servo[3].angle = 140-x
        kit.servo[0].angle = 90 + x
        time.sleep(0.04)
        if(x == 70):
            print("FOUND IT")
    if(dist_count >= 3):
        grab = True
        break
    
  #if(grab == True):
  print("GRABBING")
  kit.servo[5].angle = 175

def extend():
    print("extend() function")
    global pos4
    global pos2
    newpos4 = 0
    newpos2 = 0
    dist_count = 0
    for x in range (0,70):
        dist = distance()
        if (dist < 5 and dist > 0):
            print("if dist < 3 statement")
            dist_count = dist_count + 1
        print("dist_count = " + str(dist_count))
        if(dist_count < 3):
            kit.servo[4].angle = pos4 - x #170-x
            kit.servo[2].angle = pos2 + x #40+x
            time.sleep(0.04)
            newpos4 = pos4 - x
            newpos2 = pos2 + x
            print("newpos4 = " + str(newpos4))
            print("newpos2 = " + str(newpos2))
        if(dist_count >= 3):
            print("GRABBING")
            break
    kit.servo[5].angle = 175 #175
    pos4 = newpos4
    pos2 = newpos2


def retract():
    print("retract() function")
    global pos4
    global pos2
    print("position2 = " + str(pos2))
    newpos4 = pos4
    newpos2 = pos2
    print("newpos4 = " + str(newpos4))
    print("newpos2 = " + str(newpos2))
    for x in range (0, 70):
        kit.servo[4].angle = pos4 + x #100+x
        kit.servo[2].angle = pos2 - x #110-x
        time.sleep(0.04)
        newpos4 = pos4 + x
        newpos2 = pos2 - x
    pos4 = newpos4
    pos2 = newpos2

def retrieve():
    time.sleep(1)
    #retrieve motor 0
    drive(150,10,0.035,0)
    
    kit.servo[3].angle = 40
    time.sleep(1)
    kit.servo[5].angle = 80
    grabbing = False

if __name__ == "__main__":


  try: 
    kit.servo[0].angle = 10
    time.sleep(1)

    kit.servo[4].angle = 175

    kit.servo[3].angle = 80
    time.sleep(1)

    drive(0,180,0.035,0)

    kit.servo[2].angle = 80 
    kit.servo[4].angle = 170
    kit.servo[0].angle = 180
    kit.servo[5].angle = 80 
    extend() #extend arm farther to reach object and grab
    drive(180,0,0.035,0)
    kit.servo[5].angle = 95
    retract()

    
  except (KeyboardInterrupt, SystemExit): #when you press ctrl+c
    print("\nDone.\nExiting.")
    GPIO.output(oe, GPIO.HIGH)
    GPIO.cleanup()
