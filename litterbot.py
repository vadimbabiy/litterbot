import jetson.inference
import jetson.utils
import time
import RPi.GPIO as gpio

PIN_TRIGGER = 22
PIN_ECHO = 24
 
drive_delay = time.time() + 1
sleep_time = 0.03
gpio.setmode(gpio.BOARD)
gpio.setup(7,gpio.OUT)
gpio.setup(11,gpio.OUT)
gpio.setup(13,gpio.OUT)
gpio.setup(15,gpio.OUT)
gpio.setup(32,gpio.OUT) #pwm
gpio.setup(33,gpio.OUT) #p2m
gpio.setup(PIN_TRIGGER, gpio.OUT)
gpio.setup(PIN_ECHO, gpio.IN)

gpio.output(PIN_TRIGGER, gpio.LOW)
print("Waiting for sensor to settle")
time.sleep(2)


pwm1 = gpio.PWM(32,50)
pwm2 = gpio.PWM(33,50)

pwm1.start(40)
pwm2.start(40)
running = False

def drivetrain(drv):
      
      if (drv == 0 and running == False):
         print("drivetrain turn left")
         gpio.output(7, False)
         gpio.output(11, True)
         gpio.output(13, False)
         gpio.output(15, True)   
       
      elif (drv == 1 and running == False):
         print("drivetrain turn right")
         gpio.output(7, True)
         gpio.output(11, False)
         gpio.output(13, True)
         gpio.output(15, False)
            
      elif (drv == 3 and running == False):
         print("drivetrain move forward")
         gpio.output(7, False)
         gpio.output(11, True)
         gpio.output(13, True)
         gpio.output(15, False)
      elif (drv ==4):
         stop(sleep_time)
      else:
         gpio.cleanup()
         print("drivetrain turn right")

def stop(tf):
   gpio.output(7, False)
   gpio.output(11, False)
   gpio.output(13, False)
   gpio.output(15, False)
   time.sleep(tf)
   stopped = False
 
timeStamp=time.time()
fpsFilt=0
net=jetson.inference.detectNet('coco-bottle',threshold=.5)
dispW=1280
dispH=720
flip=2
 
# Gstreamer code for improved camera quality
camSet='nvarguscamerasrc wbmode=3 tnr-mode=2 tnr-strength=1 ee-mode=2 ee-strength=1 ! video/x-raw(memory:NVMM), width=1280, height=720, format=NV12, framerate=15/1 ! nvvidconv flip-method='+str(flip)+' ! video/x-raw, width='+str(dispW)+', height='+str(dispH)+', format=BGRx ! videoconvert ! video/x-raw, format=BGR ! videobalance contrast=1.5 brightness=-.2 saturation=1.2 ! appsink'
cam=jetson.utils.gstCamera(dispW,dispH,'0')

display=jetson.utils.glDisplay()
stopped = False
switch_to_sonar = False
arrived = False
last_op_was_turn = False
badframes = 0
turnsteps = 0
while display.IsOpen():
    print("main loop")
    img, width, height= cam.CaptureRGBA()

    decision_time = 0.1
    detections=net.Detect(img, width, height)

    if(switch_to_sonar == True):

        print("Calculating distance")

        no_object_count = 0
        N = 10
        distance = 0
        for i in range(1, N):
            gpio.output(PIN_TRIGGER, gpio.HIGH)

            time.sleep(0.00001)

            gpio.output(PIN_TRIGGER, gpio.LOW)

            test_start_time = time.time()
            pulse_end_time = test_start_time
            while gpio.input(PIN_ECHO) == 0 and (time.time() - test_start_time) < 0.06:
                pulse_start_time = time.time()
            while gpio.input(PIN_ECHO) == 1 and (time.time() - test_start_time) < 0.06:
                pulse_end_time = time.time()

            if pulse_end_time != test_start_time:
                pulse_duration = pulse_end_time - pulse_start_time
                distance = distance + round(pulse_duration * 17150, 2)
            else:
                no_object_count = no_object_count + 1

        if(no_object_count != N):
            distance /= N - no_object_count
        else:
            print("No object detected. Possible sensor failure.")
            distance = 0



        print("Distance: ", distance, "cm")
        if not arrived:
            if(distance > 26):
                print("sonar forward")
                drivetrain(3)
            else:
                print("sonar stop")
                arrived = True
                drivetrain(4)
        else:
            drivetrain(4)
            if(distance > 29):
                print("lost object. Switching back to camera.")
                arrived = False
                switch_to_sonar = False

    if(len(detections) == 0 and not switch_to_sonar):
        print("Don't see anything.")
        drivetrain(4)

    for detect in detections:
        print("Detected " +  str(len(detections)) + "items")
        ID=detect.ClassID
        top=int(detect.Top)
        left=int(detect.Left)
        bottom=int(detect.Bottom)
        right=int(detect.Right)
        item=net.GetClassDesc(ID)
        Ycent=top+dispH/2
        Xcent=left+dispW/2
        errorX=Xcent-width/2
        errorY=Xcent-height/2
        item_centerH = bottom-top
        item_centerW = left + (right-left)/2
        bottomThreshold = 600 #600 for 720p

        print("top = " + str(top) + "  bot = " + str(bottom))
        print("left = " + str(left) + "   right = " + str(right))

        if(not switch_to_sonar and bottom > bottomThreshold and not last_op_was_turn):
           print("item located")
           drivetrain(4)
           switch_to_sonar = True
        if not switch_to_sonar:
           badframes = 0
           if abs(errorX)>15:
              if (item_centerW > (dispW/2 +30)):
                 last_op_was_turn = True
                 print("Camera turn right")
                 drivetrain(1)
              if (item_centerW < (dispW/2 -30)):
                 last_op_was_turn = True
                 print("Camera turn left")
                 drivetrain(0)
              if (item_centerW >= (dispW/2-30) and item_centerW <= (dispW/2+30)):
                 print("Camera forward")
                 drivetrain(3)
                 last_op_was_turn = False
                  
              print("item_centerW=" + str(item_centerW))
        break #stop looking for objects after finding one

    display.RenderOnce(img,width,height)
    dt=time.time()-timeStamp
    timeStamp=time.time()
    fps=1/dt
    fpsFilt=.9*fpsFilt + .1*fps
    print(str(round(fps,1))+' fps')